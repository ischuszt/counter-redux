import * as at from "./types";

export const changeCounterValue = (value) => ({
    type: at.CHANGE_COUNTER_VALUE,
    value: value
})

export const changeOperationType = (value) => ({
    type: at.CHANGE_OPERATION,
    value: value
})

export const OperationTypes = {
    INCREMENT: "INCREMENT",
    DECREMENT: "DECREMENT"
}