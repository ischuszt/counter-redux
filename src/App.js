import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import Counter from "./Counter";
import { Container } from "semantic-ui-react";
//!!!!!!!
import { connect } from "react-redux";
import {
  OperationTypes,
  changeCounterValue,
  changeOperationType
} from "./actions";

const App = ({ counter, isIncrementing, setOperation, setCounter }) => {
  const handleClick = () => {
    setCounter(isIncrementing ? counter + 1 : counter - 1);
  };

  const handleChange = () => {
    setOperation(!isIncrementing);
  };

  return (
    <Container text>
      <Counter
        counter={counter}
        isIncrementing={isIncrementing}
        onChange={handleChange}
        onClick={handleClick}
      />
    </Container>
  );
};

const mapStateToProps = state => ({
  counter: state.counterValue,
  isIncrementing: state.operationType === OperationTypes.INCREMENT
});

const mapDispatchToProps = dispatch => ({
  setCounter: value => dispatch(changeCounterValue(value)),
  setOperation: isIncrementing => {
    if (isIncrementing) {
      dispatch(changeOperationType(OperationTypes.INCREMENT));
    } else {
      dispatch(changeOperationType(OperationTypes.DECREMENT));
    }
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
