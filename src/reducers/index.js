import { OperationTypes } from "../actions";
import * as at from "../actions/types"


const initialState = {
    counterValue: 0,
    operationType: OperationTypes.INCREMENT
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case at.CHANGE_COUNTER_VALUE:
            return {...state, counterValue: action.value}
            //return Object.assign({}, state, {counterValue: action.value})
        case at.CHANGE_OPERATION:
            return {...state, operationType: action.value}
        default:
            return state
    }
}

export default rootReducer;

// const squares = [...this.state.squares]