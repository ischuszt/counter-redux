import React from "react";
import { Segment, Button, Radio } from "semantic-ui-react";
import PropTypes from "prop-types";

function Counter({ isIncrementing, counter,
   onClick, onChange }) {
  return (
    <Segment>
      <p>{counter}</p>
      <Radio slider checked={isIncrementing} 
          onChange={() => onChange()} />
      <Button onClick={() => onClick()}>
        {isIncrementing ? "Increment" : "Decrement"}
      </Button>
    </Segment>
  );
}

Counter.propTypes = {
  isIncrementing: PropTypes.bool.isRequired,
  counter: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Counter;
